# gulimail

#### 介绍
{**以下是 Gitee 平台说明，您可以替换此简介**
Gitee 是 OSCHINA 推出的基于 Git 的代码托管平台（同时支持 SVN）。专为开发者提供稳定、高效、安全的云端软件开发协作平台
无论是个人、团队、或是企业，都能够用 Gitee 实现代码托管、项目管理、协作开发。企业项目请看 [https://gitee.com/enterprises](https://gitee.com/enterprises)}

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

参考文档链接：
[https://blog.csdn.net/weixin_42406046/article/details/80604623](http://https://blog.csdn.net/weixin_42406046/article/details/80604623)

参考项目链接：
[https://gitee.com/efairy520/gulimall](http://http://https://gitee.com/efairy520/gulimall)

[https://www.yuque.com/zhangshuaiyin/guli-mall](http://https://www.yuque.com/zhangshuaiyin/guli-mall)

npm install --global --production windows-build-tools

Python 2.7.15 (v2.7.15:ca079a3ea3, Apr 30 2018, 16:30:26) [MSC v.1500 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>>

1. 使用管理员身份运行CMD（权限问题|重要），进入项目的根目录
2. 删除node_modules文件夹
3. 修改项目文件 package.json 中的 saas 版本（不用太高也不能太低）
    "node-sass": "4.13.1",
     "sass-loader": "7.3.1",
4. 执行以下三条命令，项目就能跑起来了（第一条取自尚硅谷官方）
   npm install chromedriver --chromedriver_cdnurl=http://cdn.npm.taobao.org/dist/chromedriver
   npm install 
   npm run dev

5、 这样安装完后可能还会报错, 此时就需要卸载掉所有的 webpack webpack-cli webpack-dev-server

　　重新一起安装, 一定记住, 一起同时安装!!

npm i webpack webpack-cli webpack-dev-server -D

6、清空缓存

npm cache clean --force

7、##删除 #全局卸载

npm uninstall webpack -g 

#局部卸载 

npm uninstall webpack -s

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
